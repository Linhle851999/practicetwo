# practicetwo

## NOTES

## OVERVIEW
- Developer: Linh Le
- Mentor: Hoang Nguyen
- Design: figma
- Timeline 5 days
- Actual: 7 days

## REQUIREMENT

 - Use the right HTML Tags
 - Apply Flexbox and Grid
 - Use validate tools: W3 Validator
 - Getting the code to work cross browsers latest version (Chrome, Firefox, MS Edge, Opera, Safari) (*)

## TARGETS

 - Improve more ability picking HTML tags and styling solutions
 - More practice with CSS selectors
 - Learn how to make a responsive website
 - Learn how to make a website that can cross-browser

## TECHNICAL STACK
 
 - HTML5
 - CSS3

## TOOL:

 - vscode
 - git, gitlap

## PLAN
 - Implement code skeleton (3 hours)
 - Implement header (4 hours)
    - [x] Implement navigation
    - [x] Implement logo
    - [x] Implement dropdown
    - [x] Implement icon-header
 - Implement beats-studio3 section(5 hours)
    - [x] Implement image
    - [x] Implement beats-content
 - Implement beats-pill section(5 hours)
    - [x] Implement image
    - [x] Implement beats-pill-content
    - [x] Implement beats-pill product color
 - Implement powerbeats-pro section(5 hours)
    - [x] Implement image
    - [x] Implement powerbeats-pro content
 - Implement beats-x section(5 hours)
    - [x] Implement image
    - [x] Implement beats-x content
 - Implement product section(1 days)
    - [x] Implement card item-one
    - [x] Implement card item-two
    - [x] Implement card item-three
    - [x] Implement card item-four
    - [x] Implement card item-five
    - [x] Implement card item-six
 - Implement contact section(3 hours)
    - [x] Implement contact-name
    - [x] Implement contact-content
    - [x] Implement form-contact
 - Implement footer(3 hours)
    - [x] Implement logo
    - [x] Implement footer-product
    - [x] Implement footer-suport
    - [x] Implement footer-company
    - [x] Implement footer-followus


